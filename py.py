from sympy.ntheory import factorint
from sympy import mod_inverse
from Crypto.PublicKey import RSA
import time

def phiN(p,q):
    return (p-1)*(q-1)
keyFilePath,key = input("[factor] --$ Clé privée >> "),""
with open(keyFilePath, 'r') as key: key = RSA.import_key(key.read())
print("[factor] --$ DEUMS la clé est chargée")
n,e = key.n,key.e
print("[factor] --$ Lecture de n et e ")
print("[factor] --$ tentative de factorisation de n")
t1,factored = time.time(),list(factorint(n))
print("[factor] --$ c'est très nwaar n est factorisé")
deltat = time.time()-t1
print(f"[factor] --$ en {round(deltat)} secondes")
factor = [factored[0], factored[1]]
print("[factor] --$ calcule de Φn")
phi = phiN(factor[0], factor[1])
print("[factor] --$ calcule de d")
d = mod_inverse(e, phi)
print("[factor] --$ création de la clé")
privateKey = RSA.construct((n,e,d))
privateKey = privateKey.export_key()
print("[factor] --$ deums va dormir")
with open("clé_privée"+keyFilePath, "wb") as key:
    key.write(privateKey)

